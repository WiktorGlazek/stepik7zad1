**Magazyn api project**

**Endpoints:**

**get all products -->** **GET** 
/products

**get but with optional parameters -->** **GET** 
/products?filter=(...)&value=(...)

**get magazine report -->** **GET** 
/products/report

**add new product -->** **POST** 
/products

**modify product by id -->** **PUT** 
/products/:id

**delete product by id -->** **DELETE** 
/products/:id
