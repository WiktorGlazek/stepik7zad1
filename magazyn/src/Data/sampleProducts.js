const sampleProducts = [
    {
        item: "Bread",
        price: 4.55,
        note: "description of product",
        quantity: 250,
        unit: "piece"
    },
    {
        item: "Butter",
        price: 6.50,
        note: "description of product",
        quantity: 1000,
        unit: "piece"
    },
    {
        item: "Cheese",
        price: 3.20,
        note: "description of product",
        quantity: 30,
        unit: "kg"
    },
    {
        item: "Yougurt",
        price: 1.99,
        note: "description of product",
        quantity: 2200,
        unit: "piece"
    },
    {
        item: "Ham",
        price: 8.50,
        note: "description of product",
        quantity: 25,
        unit: "kg"
    },
    {
        item: "Tomato",
        price: 1.4,
        note: "description of product",
        quantity: 30,
        unit: "kg"
    },
    {
        item: "Apple",
        price: 1.1,
        note: "description of product",
        quantity: 40,
        unit: "kg"
    }
]

module.exports = sampleProducts