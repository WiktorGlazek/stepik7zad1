const express = require('express')
const router = express.Router()
const { Product } = require('../Models/Product')

router.get("/", async (req, res)=> {
    try{
        const {filter,value} = req.query
        if (filter=="item"){
            const products = await Product.find({item: value});
            res.status(200).json(products)
        }

        else if (filter=="price"){
            const products = await Product.find({price: value});
            res.status(200).json(products)
        }

        else if (filter=="quantity"){
            const products = await Product.find({quantity: value});
            res.status(200).json(products)
        }
        
        else{
            const products = await Product.find();
            res.status(200).json(products)
        }
    }
    catch(error){
        res.status(400).json({message: error.message})
    }
})

router.post("/", async (req, res)=> {
    try{
        const product = req.body
        const checkUniq = await Product.count({item: product.item})
        if (checkUniq === 0){
            const newProduct = new Product(product)
            newProduct.save()
            res.status(201).json({message: "Product saved"})
        }
        else{
            res.status(400).json({message: "Product exists"})
        }
    }
    catch(error){
        res.status(400).json({message: error.message})
    }
})

router.put("/:id", async (req, res)=> {
    try{
        const {id} = req.params
        const {item, price, note, quantity, unit} = req.body
        const toEdit = await Product.findById(id)
        if (item) toEdit.item = item
        if (price) toEdit.price = price
        if (note) toEdit.note = note
        if (quantity) toEdit.quantity = quantity
        if (unit) toEdit.unit = unit
        await toEdit.save()
        res.status(201).json({message: "Product modified"})
    }
    catch(error){
        res.status(400).json({message: error.message})
    }
})

router.delete("/:id", async (req, res)=> {
    try{
        const {id} = req.params
        const toDelete = await Product.findByIdAndDelete(id)
        if(toDelete){
            res.status(200).json({message: "Product deleted"})
        }
        else res.status(400).json({message: "No such product"})

    }
    catch(error){
        res.status(400).json({message: error.message})
    }
})

router.get("/report", async (req, res)=> {
    try{

        const productList = await Product.aggregate().
        project({_id: 0, product: "$item", available: {$concat: [ {$toString: "$quantity"}, " ", "$unit", "(s)" ]}, value: {$multiply: [ "$price", "$quantity" ]}}).
        exec()

        const netValue = await Product.aggregate().
        group({
            _id: null,
            net_value: {$sum: {$multiply: [ "$price", "$quantity" ]}}
            }).
        project({_id:0, net_value: 1}).
        exec()

        res.status(200).json({List_of_products:productList, Total_products_value: netValue})
    }
    catch(error){
        res.status(400).json({message: error.message})
    }
})

module.exports = router