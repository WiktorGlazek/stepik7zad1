const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    item: {
        type: String,
    },
    price: {
        type: Number,
    },
    note: {
        type: String,
    },
    quantity: {
        type: Number,
    },
    unit: {
        type: String,
    }
});

const Product = mongoose.model('products', ProductSchema);
module.exports = { Product };