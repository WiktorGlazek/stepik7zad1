const express = require('express');
const {mongoose} = require('mongoose');
const sampleProducts  = require('./src/Data/sampleProducts')
const { Product } = require('./src/Models/Product')
const ProducsController = require('./src/Controllers/ProducsController')

const app = express();
app.use(express.json());

const start = async () => {
    try {
        await mongoose.connect("****private_mongo_uri****")
        await Product.collection.drop()
        await Product.insertMany(sampleProducts)
        app.listen(3001)
    }
    catch (error) {
       console.log(error); 
    }  
}

app.use('/products', ProducsController)

start();